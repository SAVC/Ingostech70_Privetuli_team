package team.privetuli.ingostech.service;

import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;
import team.privetuli.ingostech.dao.Migration;
import team.privetuli.ingostech.model.CompositeKey;
import team.privetuli.ingostech.model.SignificantByThresholdAndCoverage;
import team.privetuli.ingostech.model.dto.ExecutionResultDTO;

import java.util.Arrays;
import java.util.List;

@Log4j
@Service
public class TestDataServiceImpl implements TestDataService {

    private final TraceMatrixService traceMatrixService;

    public TestDataServiceImpl(TraceMatrixService traceMatrixService) {
        this.traceMatrixService = traceMatrixService;
    }

    @Override
    public ExecutionResultDTO run() {
//        log.info("Started");

        // TODO вместо этого куска кода парсить данные из файла(Денис) и отправлять софрмированную структуру в мой сервис
        // данные для медов сервиса должны быть сформированны при или после парсинга файла.
        traceMatrixService.createGraphDatabase(false);
        List<CompositeKey> compositeKeys = Arrays.asList(
                new CompositeKey(Migration.TEST_MODULE, 0L),
                new CompositeKey(Migration.TEST_MODULE, 1L),
                new CompositeKey(Migration.TEST_MODULE, 2L),
                new CompositeKey(Migration.TEST_MODULE, 3L),
                new CompositeKey(Migration.TEST_MODULE, 4L),
                new CompositeKey(Migration.TEST_MODULE, 5L),
                new CompositeKey(Migration.TEST_MODULE, 6L),
                new CompositeKey(Migration.TEST_MODULE, 7L),
                new CompositeKey(Migration.TEST_MODULE, 8L),
                new CompositeKey(Migration.TEST_MODULE, 9L));
        List<Long> runIdsList = Arrays.asList(0L, 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L);

        // Start optimization
        traceMatrixService.calculateNodesWeight(compositeKeys);
        List<Long> sortedByPriorityWeights = traceMatrixService.getSortedByPriorityWeights(runIdsList);
        SignificantByThresholdAndCoverage significantByThreshold = traceMatrixService.getSignificantByThreshold(sortedByPriorityWeights, 0.05);
        List<Long> significantRunIds = sortedByPriorityWeights.subList(0, significantByThreshold.getCount());

        System.out.println(sortedByPriorityWeights);
        System.out.println(significantByThreshold.getCount());

        System.out.println("Result RunIds" + significantRunIds);
        return new ExecutionResultDTO(significantByThreshold.getCoverage(), significantRunIds);
    }
}
