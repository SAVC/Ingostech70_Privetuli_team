package team.privetuli.ingostech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.privetuli.ingostech.dao.Migration;
import team.privetuli.ingostech.dao.model.TraceMatrixNode;
import team.privetuli.ingostech.dao.repository.TraceMatrixRepository;
import team.privetuli.ingostech.model.CompositeKey;
import team.privetuli.ingostech.model.RunIdWeight;
import team.privetuli.ingostech.model.SignificantByThresholdAndCoverage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TraceMatrixServiceImpl implements TraceMatrixService {

    private final Migration migration;
    private final TraceMatrixRepository traceMatrixRepository;

    @Autowired
    public TraceMatrixServiceImpl(Migration migration, TraceMatrixRepository traceMatrixRepository) {
        this.migration = migration;
        this.traceMatrixRepository = traceMatrixRepository;
    }


    @Override
    public void createGraphDatabase(boolean skip) {
        if (!skip) {
            migration.execute();
        }
    }

    @Override
    public void calculateNodesWeight(List<CompositeKey> compositeKeys) {
        for (CompositeKey compositeKey : compositeKeys) {
            Long sum = 0L;
            List<TraceMatrixNode> byRunId = traceMatrixRepository.getByModuleNameAndStringNumber(compositeKey.getModule(), compositeKey.getStringNumber());
            for (TraceMatrixNode traceMatrixNode : byRunId) {
                sum += traceMatrixNode.getIsCalled() ? 1L : 0L;
            }
            for (TraceMatrixNode traceMatrixNode : byRunId) {
                traceMatrixNode.setWeight((traceMatrixNode.getIsCalled() ? 1.0 : 0.0) / sum);
            }
            traceMatrixRepository.save(byRunId);
        }
    }

    @Override
    public List<Long> getSortedByPriorityWeights(List<Long> runIds) {
        List<RunIdWeight> runIdWeights = new ArrayList<>();
        for (Long runId : runIds) {
            List<TraceMatrixNode> byRunId = traceMatrixRepository.getByRunId(runId);
            Double weight = 0.0;
            for (TraceMatrixNode traceMatrixNode : byRunId) {
                weight += traceMatrixNode.getWeight();
            }
            runIdWeights.add(new RunIdWeight(runId, weight));
        }
        Collections.sort(runIdWeights);
        return runIdWeights.stream().map(RunIdWeight::getRunId).collect(Collectors.toList());
    }

    @Override
    public SignificantByThresholdAndCoverage getSignificantByThreshold(List<Long> sortedRunIds, double threshold) {
        int count = 1;
        boolean[] mask = getMask(traceMatrixRepository.getByRunId(sortedRunIds.get(0)));
        double coverage = coverage(mask);
        System.out.println("Coverage " + coverage);

        for (int index = 1; index < sortedRunIds.size(); index++) {
            Long runId = sortedRunIds.get(index);
            List<TraceMatrixNode> byRunId = traceMatrixRepository.getByRunId(runId);
            boolean[] nextMask = getMask(byRunId);
            boolean[] sum = maskSum(mask, nextMask);
            double newCoverage = coverage(sum);
            if(Math.abs(newCoverage - coverage) < threshold){
                System.out.println("Diff = " + Math.abs(newCoverage - coverage));
                break;
            }
            mask = sum;
            coverage = newCoverage;
            System.out.println("Coverage " + coverage);
            count++;
        }

        return new SignificantByThresholdAndCoverage(count, coverage);
    }

    private boolean[] getMask(List<TraceMatrixNode> traceMatrixNodes) {
        boolean[] result = new boolean[traceMatrixNodes.size()];
        for (int index = 0; index < traceMatrixNodes.size(); index++) {
            result[index] = traceMatrixNodes.get(index).getIsCalled();
        }
        return result;
    }

    private boolean[] maskSum(boolean[] first, boolean[] seccond) {
        boolean[] result = new boolean[first.length];
        for (int index = 0; index < first.length; index++) {
            result[index] = first[index] || seccond[index];
        }
        return result;
    }

    private double coverage(boolean[] mask) {
        double count = 0.0;
        for (int index = 0; index < mask.length; index++) {
            if (mask[index]) {
                count++;
            }
        }
        return count / mask.length;
    }

    // TODO replace by springData
    private TraceMatrixNode getStartNode() {
        return traceMatrixRepository.getByRunIdAndModuleNameAndStringNumber(0L, Migration.TEST_MODULE, 0L);
    }

}
