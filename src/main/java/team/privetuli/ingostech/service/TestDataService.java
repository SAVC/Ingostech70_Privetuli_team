package team.privetuli.ingostech.service;

import team.privetuli.ingostech.model.dto.ExecutionResultDTO;

public interface TestDataService {
    ExecutionResultDTO run();
}
