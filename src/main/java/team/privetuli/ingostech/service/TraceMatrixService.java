package team.privetuli.ingostech.service;

import team.privetuli.ingostech.model.CompositeKey;
import team.privetuli.ingostech.model.SignificantByThresholdAndCoverage;

import java.util.List;

public interface TraceMatrixService {

    void createGraphDatabase(boolean skip);

    void calculateNodesWeight(List<CompositeKey> runIds);

    List<Long> getSortedByPriorityWeights(List<Long> runId);

    /**
     * Get significant rows count by threshold
     *
     * @param sortedRunIds - sorted list
     * @param threshold    - minimal percentage for iteration
     * @return count
     */
    SignificantByThresholdAndCoverage getSignificantByThreshold(List<Long> sortedRunIds, double threshold);
}
