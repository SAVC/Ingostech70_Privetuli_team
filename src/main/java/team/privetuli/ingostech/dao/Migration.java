package team.privetuli.ingostech.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.privetuli.ingostech.dao.model.TraceMatrixNode;
import team.privetuli.ingostech.dao.repository.TraceMatrixRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class Migration {

    public static final String TEST_MODULE = "TEST_MODULE";

    private final TraceMatrixRepository traceMatrixRepository;

    @Autowired
    public Migration(TraceMatrixRepository traceMatrixRepository) {
        this.traceMatrixRepository = traceMatrixRepository;
    }

    public void execute() {
        traceMatrixRepository.deleteAll();


        List<Long> runIdOrder = new ArrayList<>();
        List<Long> compositeKeyOrder = new ArrayList<>();

        for (long runId = 0; runId < 10; runId++) {
            runIdOrder.add(runId);
            for (long compositeKey = 0; compositeKey < 10; compositeKey++) {
                compositeKeyOrder.add(compositeKey);
                TraceMatrixNode traceMatrixNode = new TraceMatrixNode();
                // Set data
                traceMatrixNode.setRunId(runId);
                traceMatrixNode.setModuleName(TEST_MODULE);
                traceMatrixNode.setStringNumber(compositeKey);
                traceMatrixNode.setIsCalled(Math.random() > 0.5);
                traceMatrixRepository.save(traceMatrixNode);

                // Set relations
                if (runId > 0) {
                    TraceMatrixNode byRunId = traceMatrixRepository.getByRunIdAndModuleNameAndStringNumber(runId - 1, TEST_MODULE, compositeKey);
                    traceMatrixNode.setPrevRunId(byRunId);
                    byRunId.setNextRunId(traceMatrixNode);
                    traceMatrixRepository.save(byRunId);
                }

                if (compositeKey > 0) {
                    TraceMatrixNode byModuleNameAndStringNumber = traceMatrixRepository.getByRunIdAndModuleNameAndStringNumber(runId, TEST_MODULE, compositeKey - 1);
                    traceMatrixNode.setPrevCompositeKey(byModuleNameAndStringNumber);
                    byModuleNameAndStringNumber.setNextCompositeKey(traceMatrixNode);
                    traceMatrixRepository.save(byModuleNameAndStringNumber);
                }

                if (runId > 0 || compositeKey > 0) {
                    traceMatrixRepository.save(traceMatrixNode);
                }
            }
        }


        System.out.println("Finish");
    }
}
