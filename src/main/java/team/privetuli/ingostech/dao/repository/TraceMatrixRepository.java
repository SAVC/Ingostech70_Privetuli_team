package team.privetuli.ingostech.dao.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import team.privetuli.ingostech.dao.model.TraceMatrixNode;

import java.util.List;

public interface TraceMatrixRepository extends GraphRepository<TraceMatrixNode> {

    List<TraceMatrixNode> getByRunId(Long runId);

    List<TraceMatrixNode> getByModuleNameAndStringNumber(String moduleName, Long stringNumber);

    TraceMatrixNode getByRunIdAndModuleNameAndStringNumber(Long runId, String moduleName, Long stringNumber);


    List<TraceMatrixNode> findByPrevCompositeKeyIsNull();
}
