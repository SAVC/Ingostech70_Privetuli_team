package team.privetuli.ingostech.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExecutionResultDTO {
    private Double coverage;
    List<Long> methodsRunIds;
}
