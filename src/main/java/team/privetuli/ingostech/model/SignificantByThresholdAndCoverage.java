package team.privetuli.ingostech.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignificantByThresholdAndCoverage {
    private Integer count;
    private Double coverage;
}
